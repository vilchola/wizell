﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity;

namespace WinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtSearch.Select();
            gbGame.Hide();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            loadGV(0);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        private void loadGV(int id)
        {
            using (var db = new Models.wizell())
            {
                var query = from hist in db.Histories
                             where hist.ClientId == id || id == 0
                             group hist by new { hist.Client, hist.Game } into grp
                             orderby grp.Key.Game.Name
                             select new
                             {
                                 ClientId = grp.Key.Client.Id,
                                 Client = grp.Key.Client.Name,
                                 GameId = grp.Key.Game.Id,
                                 Game = grp.Key.Game.Name,
                                 Count = grp.Count(),
                                 LastDate = grp.Max(d => d.Date)
                             };
                gvHistory.DataSource = query.ToList();
                gvHistory.AutoResizeColumns();
                gvHistory.Columns["ClientId"].Visible = false;
                gvHistory.Columns["GameId"].Visible = false;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                lblError.Text = "";
                int id = 0;
                if (!string.IsNullOrEmpty(txtSearch.Text.Trim()))
                    id = int.Parse(txtSearch.Text.Trim());
                loadGV(id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                lblError.Text = string.Format("Attention: {0}", ex.Message);
                gbGame.Hide();
            }
        }

        private void gvHistory_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gvHistory.Rows.Count > 0)
            {
                var ctx = new Models.wizell();
                var game = new Models.Game();
                try
                {
                    foreach (DataGridViewRow row in gvHistory.SelectedRows)
                    {
                        game = ctx.Games.Find(int.Parse(row.Cells[2].Value.ToString()));
                        txtId.Text = game.Id.ToString();
                        txtName.Text = game.Name;
                        txtDescription.Text = game.Description;
                        cbType.SelectedItem = game.Type;

                        gbGame.Show();
                        ctx.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    lblError.Text = string.Format("Attention: {0}", ex.Message);
                    gbGame.Hide();
                }
                finally
                {
                    ctx.Dispose();
                }
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var ctx = new Models.wizell();
            var game = new Models.Game();
            try
            {
                game = ctx.Games.Find(int.Parse(txtId.Text));
                if (!string.IsNullOrEmpty(txtName.Text.Trim()))
                {
                    if (cbType.SelectedItem != null)
                    {
                        game.Name = txtName.Text.Trim();
                        game.Description = txtDescription.Text.Trim();
                        game.Type = cbType.SelectedItem.ToString();
                        if (ctx.SaveChanges() > 0)
                        {
                            MessageBox.Show("Game updated!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
                            lblError.Text = "";
                            gbGame.Hide();
                            btnSearch_Click(sender, e);
                        }
                    }
                    else
                    {
                        lblError.Text = string.Format("Attention: The field Type can not be empty.");
                        return;
                    }
                }
                else
                {
                    lblError.Text = string.Format("Attention: The field Name can not be empty.");
                    return;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                lblError.Text = string.Format("Attention: {0}", ex.Message);
            }
            finally
            {
                ctx.Dispose();
            }
        }
    }
}
