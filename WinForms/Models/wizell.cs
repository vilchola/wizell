namespace WinForms.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class wizell : DbContext
    {
        public wizell()
            : base("name=wizellContext")
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Game> Games { get; set; }
        public virtual DbSet<History> Histories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Address)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<Client>()
                .HasMany(e => e.Histories)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Game>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .Property(e => e.Type)
                .IsUnicode(false);

            modelBuilder.Entity<Game>()
                .HasMany(e => e.Histories)
                .WithRequired(e => e.Game)
                .WillCascadeOnDelete(false);
        }
    }
}
