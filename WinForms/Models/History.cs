namespace WinForms.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class History
    {
        //private readonly ObservableListSource<Client> _clients = new ObservableListSource<Client>();
        //private readonly ObservableListSource<Game> _games = new ObservableListSource<Game>();

        public int Id { get; set; }
        public int ClientId { get; set; }
        public int GameId { get; set; }
        public DateTime Date { get; set; }
        public virtual Client Client { get; set; }
        public virtual Game Game { get; set; }

        //public virtual ObservableListSource<Client> Clients { get { return _clients; } }
        //public virtual ObservableListSource<Game> Games { get { return _games; } }
    }
}
