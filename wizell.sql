CREATE DATABASE wizell;
GO

USE wizell
GO

CREATE TABLE Clients (
    Id      INT IDENTITY (1, 1),
    Name    VARCHAR(32) NOT NULL,
    Address VARCHAR(256) NOT NULL,
    Phone   VARCHAR(11) NOT NULL
)
GO

ALTER TABLE Clients
	ADD CONSTRAINT PK_Clients PRIMARY KEY CLUSTERED (Id ASC)
GO

CREATE TABLE Games (
    Id          INT IDENTITY (1, 1),
    Name        VARCHAR(60) NOT NULL,
    Description VARCHAR(256) NULL,
    Type        VARCHAR(10) NOT NULL
)
GO

ALTER TABLE Games
	ADD CONSTRAINT PK_Games PRIMARY KEY CLUSTERED (Id ASC)
GO

CREATE TABLE Histories (
    Id       INT IDENTITY (1, 1),
    ClientId INT NOT NULL,
    GameId   INT NOT NULL,
    Date     DATETIME NOT NULL
)
GO

ALTER TABLE Histories
	ADD CONSTRAINT PK_Histories PRIMARY KEY CLUSTERED (Id ASC)
GO

ALTER TABLE Histories
	ADD CONSTRAINT FK_Histories_Clients_ClientId FOREIGN KEY (ClientId) REFERENCES Clients (Id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

ALTER TABLE Histories
	ADD CONSTRAINT FK_Histories_Games_GameId FOREIGN KEY (GameId) REFERENCES Games (Id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
GO

/**/

INSERT INTO Clients (Name, Address, Phone) VALUES ('Henry', 'Ma�ongo', '04144174545'), ('Alejandro', 'El Socorro', '04127830147'), ('Roberto', 'Valencia', '04140402311')
GO

INSERT INTO Games (Name, Description, Type) VALUES ('Crash Bandicoot', NULL, 'Aventura'), ('Crash Bandicoot 2', NULL, 'Aventura'), ('Crash Bandicoot 3', NULL, 'Aventura'), ('Ace Combat 2', NULL, 'Simulaci�n'), ('American Truck Simulator', NULL, 'Simulaci�n'), ('Los Sims', NULL, 'Simulaci�n'), ('Resident Evil', NULL, 'Terror'), ('Resident Evil 2', NULL, 'Terror'), ('Resident Evil 3', NULL, 'Terror'), ('Final Fantasy VII', NULL, 'RPG'), ('Final Fantasy VIII', NULL, 'RPG'), ('Final Fantasy IX', NULL, 'RPG')
GO

INSERT INTO Histories (ClientId, GameId, Date) VALUES (1, 1, '2018-03-01 00:00:00'), (2, 2, '2018-03-02 00:00:00'), (3, 3, '2018-03-03 00:00:00'), (1, 4, '2018-03-04 00:00:00'), (2, 5, '2018-03-05 00:00:00'), (3, 6, '2018-03-06 00:00:00'), (1, 7, '2018-03-07 00:00:00'), (2, 8, '2018-03-08 00:00:00'), (3, 9, '2018-03-09 00:00:00'), (1, 10, '2018-03-10 00:00:00'), (2, 11, '2018-03-11 00:00:00'), (3, 12, '2018-03-12 00:00:00'), (1, 1, '2018-03-13 00:00:00'), (2, 2, '2018-03-14 00:00:00'), (3, 3, '2018-03-15 00:00:00'), (1, 4, '2018-03-16 00:00:00'), (2, 5, '2018-03-17 00:00:00'), (3, 6, '2018-03-18 00:00:00'), (1, 7, '2018-03-19 00:00:00'), (2, 8, '2018-03-20 00:00:00'), (3, 9, '2018-03-21 00:00:00'), (1, 10, '2018-03-22 00:00:00'), (2, 11, '2018-03-23 00:00:00'), (3, 12, '2018-03-24 00:00:00'), (1, 2, '2018-03-25 00:00:00'), (2, 3, '2018-03-26 00:00:00'), (3, 4, '2018-03-27 00:00:00'), (1, 5, '2018-03-28 00:00:00'), (2, 6, '2018-03-29 00:00:00'), (3, 7, '2018-03-30 00:00:00')
GO